"""Данный модуль позволяет переводить предложение. Словарь слов ограничен. Пример для ввода: идти я рынок, cute he sea on now."""
from rsplit import *
from typing import List, Dict

dic_rus:Dict[str,str] = {}
dic_eng:Dict[str,str] = {}
dict_mean:Dict[str,list] = {}
temp:List[str] = []
sentence: str = rsplit_tail(input('Введите ваше предложение: \n').strip().lower(),[' ',',','.','!','?'])

def OpenTxtFile():
    """Служит для чтения файла из .txt и создания словаря для переводчика"""
    with open('Data.txt','r',encoding='utf-8-sig') as f:
        for line in f:
            step = line.strip().split(':')
            dic_rus[step[0]],dic_eng[step[1]] = step[1],step[0]
    with open('DataMean.txt','r',encoding='utf-8-sig') as f:
        for line in f:
            step = line.strip().split(',')
            dict_mean[step[0]] = [step[i] for i in range(1,len(step))]

def razdelitel(translate_sentence,temp):
        """Данная функция расставляет слова в правильном порядке, чтобы получилось внятное предложение
        Arguments:
            translate_sentencee {[List]} -- Собственно само предложение, которые вы хотите нормализовать
        Return:
            List[str] - список слов в правильном порядке
        """
        for values_list in dict_mean.values():
            temp += [word for word in translate_sentence if word in values_list] 
        return temp

perevodchik = lambda word: dic_rus[word] if word in dic_rus else dic_eng[word] #Функция переводчик




OpenTxtFile()
translate_sentence = list(map(perevodchik,sentence))
true_sentence = ' '.join(list(razdelitel(translate_sentence,temp)))
print(true_sentence)




